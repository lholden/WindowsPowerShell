Set-PSReadlineOption -ExtraPromptLineCount 1
Set-PSReadlineOption -EditMode Emacs 
Set-PSReadlineOption -HistoryNoDuplicates
Set-PSReadlineOption -BellStyle None

Set-PSReadlineKeyHandler -Key Shift+Ctrl+C -Function Copy
Set-PSReadlineKeyHandler -Key Shift+Ctrl+V -Function Paste
Set-PSReadlineKeyHandler -Key Shift+Ctrl+X -Function Cut