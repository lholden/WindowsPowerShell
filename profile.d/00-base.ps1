function Assert-Module-Installed {
  if (-not (Get-Module -List $args[0] -ErrorAction SilentlyContinue)) {
    Write-Host "Installing module $($args[0])" -BackgroundColor DarkBlue -ForegroundColor Yellow
    Install-Module $args[0] -Scope CurrentUser -Force
  }
}

function Assert-Module {
  Assert-Module-Installed $args[0]
  Import-Module $args[0]
}

function Test-Admin {
  ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
}

function Which {
  (Get-Command ${args}).definition
}
